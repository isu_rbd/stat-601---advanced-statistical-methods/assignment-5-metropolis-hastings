---
title: "STAT 601 | HW 5"
author: "Ricardo Batista"
date: "2/29/2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse); library(readr); library(knitr)
library(kableExtra); library(gridExtra)
library(latex2exp); library(mvtnorm)
```

# Question 1

&nbsp;&nbsp;&nbsp;&nbsp; The likelihood is

$$
f(y \vert \alpha, \beta)
=
\prod_{i = 1}^n f(y_i \vert \alpha, \beta)
=
\prod_{i = 1}^n \frac{\beta^{\alpha}}{\Gamma(\alpha)} y_i^{\alpha - 1} \exp\left\{-\beta y_i\right\}
=
\frac{\beta^{n\alpha}}{\Gamma(\alpha)^n}
\left[ \prod_{i = 1}^n y_i^{\alpha - 1}\right] 
\exp\left\{-\beta \sum_{i = 1}^n y_i\right\}
$$

so

$$
f(\alpha, \beta \vert y)
\propto
\left[
\frac{\beta^{n\alpha}}{\Gamma(\alpha)^n}
\left[ \prod_{i = 1}^n y_i^{\alpha - 1}\right] 
\exp\left\{-\beta \sum_{i = 1}^n y_i\right\}
\right]
\left[
\frac{1}{B}I(0 < \alpha < B)
\right]
\left[
\frac{\beta_0^{\alpha_0}}{\Gamma(\alpha_0)} \beta^{\alpha_0 - 1} \exp\left\{-\beta_0 \beta\right\}
\right]
$$

for $\alpha > 0$, $\beta > 0$, $B > 0$, $\alpha_0 > 0$, and $\beta_0 > 0$.  

# Question 2

$$
f(\alpha \vert \beta, y)
\propto
f(y \vert \alpha, \beta) f(\alpha) f(\beta)
\propto
\left[
\frac{\beta^{n\alpha}}{\Gamma(\alpha)^n} 
\left[ \prod_{i = 1}^n y_i^{\alpha - 1}\right] 
\right]
\left[ \frac{1}{B}I(0 < \alpha < B) \right]
$$

$$
\begin{aligned}
f(\beta \vert \alpha, y)
\propto
f(y \vert \alpha, \beta) f(\alpha) f(\beta)
&\propto
\left[
\beta^{n\alpha}
\exp\left\{-\beta \sum_{i = 1}^n y_i\right\}
\right]
\left[
\beta^{\alpha_0 - 1} \exp\left\{-\beta_0 \beta\right\}
\right]
\\[10pt]
&=
\beta^{\alpha_0 + n\alpha - 1}
\exp\left\{-\beta \left(\beta_0 + \sum_{i = 1}^n y_i \right)\right\}
\end{aligned}
$$

which we recognize as the kernel of a $\text{Gamma} (\alpha_0 + n\alpha, \beta_0 + \sum_{i = 1}^n y_i)$.  

\pagebreak

# Question 3

&nbsp;&nbsp;&nbsp;&nbsp; Recall that the goal of the Metropolis-Hastings algorithm is to produce a large enough sample (e.g., of size $M$) from $f(\alpha, \beta \vert y)$. We accomplish this by proposing the two-tuple $(\alpha^*, \beta^*)$ as a candidate sample from the posterior and accepting it as such using the so-called "acceptance probability". Recall that, the distribution of the accepted samples does not typically immediately converge to the target distribution, so we choose burn-in value $B$. Let $\theta \equiv (\alpha, \beta)^T$. To initialize the chain, choose starting value $\theta^{(0)} = (\alpha^{(0)}, \beta^{(0)})^T$. Then, for $m = 1, ..., B + M$  

1. Simulate

$$
(\theta^*) \sim J\left(\theta^* \vert \theta^{(m)}\right),
$$

where $J$ is an arbitrary bivariate proposal distribution for $(\alpha, \beta)$.  

2. Define $\theta^{(m + 1)} = \theta^{*}B + \theta^{(m)}(1 - B)$ where $B \sim \text{Bernoulli} \left(\tau(\theta^{(m)}, \theta^*)\right)$ and let

$$
\tau(\alpha^{(m)}, \alpha^*) 
= 
\min 
\left\{
\frac{
f(y \vert \alpha^*, \beta^*) f(\alpha^*) f(\beta^*) J(\alpha^{(m)}, \beta^{(m)} \vert \alpha^*, \beta^*)
}{
f(y \vert \alpha^{(m)}, \beta^{(m)}) f(\alpha^{(m)}) f(\beta^{(m)})J (\alpha^*, \beta^* \vert \alpha^{(m)}, \beta^{(m)})
}
, 1\right\}.
$$

be the acceptance probability. Note the ratio above is the simplified version of the expression

$$
\begin{aligned}
\frac{
f(\theta^* \vert y)
J(\theta^{(m)} \vert \theta^*)
}{
f(\theta^{(m)} \vert y)
J (\theta^* \vert \theta^{(m)})
}
&= \frac{
\frac{
f(y \vert \theta^*) f(\theta^*)
}{
f(y)
}
J(\theta^{(m)} \vert \theta^*)
}{
\frac{
f(y \vert \theta^{(m)}) f(\theta^{(m)})
}{
f(y)
}
J (\theta^* \vert \theta^{(m)})
}
=
\frac{
f(y \vert \theta^*) f(\theta^*)J(\theta^{(m)} \vert \theta^*)
}{
f(y \vert \theta^{(m)}) f(\theta^{(m)})J (\theta^* \vert \theta^{(m)})
}
\\[10pt]
&=
\frac{
f(y \vert \alpha^*, \beta^*) f(\alpha^*, \beta^*)J(\alpha^{(m)}, \beta^{(m)} \vert \alpha^*, \beta^*)
}{
f(y \vert \alpha^{(m)}, \beta^{(m)}) f(\alpha^{(m)}, \beta^{(m)})J (\alpha^*, \beta^* \vert \alpha^{(m)}, \beta^{(m)})
}
\\[10pt]
&=
\frac{
f(y \vert \alpha^*, \beta^*) f(\alpha^*) f(\beta^*) J(\alpha^{(m)}, \beta^{(m)} \vert \alpha^*, \beta^*)
}{
f(y \vert \alpha^{(m)}, \beta^{(m)}) f(\alpha^{(m)}) f(\beta^{(m)})J (\alpha^*, \beta^* \vert \alpha^{(m)}, \beta^{(m)})
}.
\end{aligned}
$$

3. Simulate from said Bernoulli distribution and record $\theta^{(m + 1)}$.  

&nbsp;&nbsp;&nbsp;&nbsp; Note we are interested in an acceptance rate roughly between $20$ and $50\%$, so we might consider different proposal distributions.  

# Question 4

&nbsp;&nbsp;&nbsp;&nbsp; Note that, generally, for multivariate distributions, the covariance of the proposal distribution should reflect the covariance structure of the target. Therefore, our proposal distribution for $(\alpha, \beta)$ could have the same form of the asymptotic distribution of the MLE, namely

$$
(\alpha^*, \beta^*)
\sim
N\left(
\begin{pmatrix}
\alpha^{(m)} \\ \beta^{(m)}
\end{pmatrix},
\frac{1}{n}
\begin{bmatrix}
\psi_1(\alpha^{(m)}) & -\frac{1}{\beta^{(m)}}\\
-\frac{1}{\beta^{(m)}} & \frac{\alpha^{(m)}}{(\beta^{(m)})^2}
\end{bmatrix}^{-1}
\right)
$$

Our initial value $(\alpha^{(0)}, \beta^{(0))^T$ would be $(\alpha_{MLE}, \beta_{MLE})^T$.  

# Question 5

&nbsp;&nbsp;&nbsp;&nbsp; Given we do not have an easy way to sample from the full conditional of $\alpha$ and wish to use the asymmetric proposal distributions above, we opt for a Metropolis-Hastings-within-Gibbs sampler. As above, the goal of this algorithm is to produce a large enough sample (e.g., of size $M$) from $f(\alpha, \beta \vert y)$. We accomplish this by proposing the value $\alpha^*$ as a candidate sample from $\alpha$'s full conditional and accepting it using acceptance probability $\tau_{\alpha}$. We then propose the value $\beta^*$ as a candidate sample from $\beta$'s full conditional and accept it using acceptance probability $\tau_{\beta}$, which takes into consideration $\alpha^{(m)}$.

&nbsp;&nbsp;&nbsp;&nbsp; Again, recall that the distribution of the accepted samples does not typically immediately converge to the target distribution, so we choose burn-in value $B$. Let $\theta \equiv (\alpha, \beta)^T$. To initialize the chain, choose starting value $\theta^{(0)} = (\alpha^{(0)}, \beta^{(0)})^T$. Then, for $m = 1, ..., B + M$  

1. Simulate

$$
\alpha^* \sim J_\alpha(\alpha^* \vert \alpha^{(m)}) 
= 
\text{uniform}\left(\max\{\alpha^{(m)} - \delta_{\alpha}, 0\}, \min\{\alpha^{(m)} + \delta_{\alpha}, B\} \right).
$$

2. Define $\alpha^{(m + 1)} = \alpha^{*}B_{\alpha} + \alpha^{(m)}(1 - B_{\alpha})$ where $B_{\alpha} \sim \text{Bernoulli} \left(\tau(\alpha^{(m)}, \alpha^*)\right)$ and 

$$
\tau(\alpha^{(m)}, \alpha^*) 
= 
\min 
\left\{
\frac{
f(y \vert \alpha^*, \beta^{(m)}) f(\alpha^*) J_\alpha(\alpha^{(m)} \vert \alpha^*)
}{
f(y \vert \alpha^{(m)}, \beta^{(m)}) f(\alpha^{(m)}) J_\alpha (\alpha^* \vert \alpha^{(m)})
}
, 1\right\}.
$$

be the acceptance probability. Note the ratio above is the simplified version of the expression

$$
\begin{aligned}
\frac{
f(\alpha^* \vert \beta^{(m)}, y)
J_\alpha(\alpha^{(m)} \vert \alpha^*)
}{
f(\alpha^{(m)} \vert \beta^{(m)} y)
J_\alpha (\alpha^* \vert \alpha^{(m)})
}
&= \frac{
\frac{
f(y \vert \alpha^*, \beta^{(m)}) f(\alpha^*, \beta^{(m)})
}{
f(\beta^{(m)}, y)
}
J_\alpha(\alpha^{(m)} \vert \alpha^*)
}{
\frac{
f(y \vert \alpha^{(m)}, \beta^{(m)}) f(\alpha^{(m)}, \beta^{(m)})
}{
f(\beta^{(m)}, y)
}
J_\alpha (\alpha^* \vert \alpha^{(m)})
}
=
\frac{
f(y \vert \alpha^*, \beta^{(m)}) f(\alpha^*) f(\beta^{(m)}) J_\alpha(\alpha^{(m)} \vert \alpha^*)
}{
f(y \vert \alpha^{(m)}, \beta^{(m)}) f(\alpha^{(m)}) f(\beta^{(m)}) J_\alpha (\alpha^* \vert \alpha^{(m)})
}
\\[10pt]
&=
\frac{
f(y \vert \alpha^*, \beta^{(m)}) f(\alpha^*) J_\alpha(\alpha^{(m)} \vert \alpha^*)
}{
f(y \vert \alpha^{(m)}, \beta^{(m)}) f(\alpha^{(m)}) J_\alpha (\alpha^* \vert \alpha^{(m)})
}.
\end{aligned}
$$

3. Simulate from said Bernoulli distribution and record $\alpha^{(m + 1)}$.

4. Simulate 

$$
\beta^* \sim J_\beta(\beta^* \vert \beta^{(m)}) 
= 
\text{uniform}\left(\max\{\beta^{(m)} - \delta_{\beta}, 0\}, \beta^{(m)} + \delta_{\beta} \right).
$$

5. Define $\beta^{(m + 1)} = \beta^{*}B_{\beta} + \beta^{(m)}(1 - B_{\beta})$ where $B_{\beta} \sim \text{Bernoulli} \left(\tau(\beta^{(m)}, \beta^*)\right)$ and 

$$
\tau(\beta^{(m)}, \beta^*) 
= 
\min 
\left\{
\frac{
f(\beta^* \vert \alpha^{(m + 1)}, y)
J_\beta(\beta^{(m)} \vert \beta^*)
}{
f(\beta^{(m)} \vert \alpha^{(m + 1)} y)
J_\beta (\beta^* \vert \beta^{(m)})
}
, 1\right\}.
$$

Note we used the full conditional for $\beta$ since we know it.  

# Question 6

&nbsp;&nbsp;&nbsp;&nbsp; We chose to implement the Metropolis-within-Gibbs algorithm with 10 chains. Maximum likelihood estimation informed our prior parameter choices:   

- $\alpha_0 = 0.86/2 = 0.43$ and $\beta_0 = 1/2$: note $\beta_{MLE} \approx 0.86$, so the foregoing parameters entail a diffuse prior centered at the MLE.
- $B = 40$: note $\alpha_{MLE} \approx 9$ but we're using a relatively diffuse prior for $beta$ and starting values for alpha in the 2.5 to 32 range.  

\vspace{15pt}

```{r echo=TRUE, fig.align="center", fig.height=4, fig.width=8, message=FALSE, warning=FALSE, cache=TRUE, results='asis'}

# General
M <- 1e4
BURN <- 250
NUM_CHAINS <- 10
output <- list()

# Data
gammadat <- read_delim("gammadat.txt", delim = " ")
y <- pull(gammadat, y); n <- length(y)
paramNames <- c("alpha", "beta")

# EDA
score_alpha <- function(alpha, y){
  n <- length(y)
  abs(n*(log(alpha) - log(mean(y)) - digamma(alpha) + mean(log(y))))
}
alpha_MLE <- nlm(score_alpha, 4, y)$estimate
beta_MLE <- alpha_MLE/mean(y)


# Distributions

## Likelihood
lik  <- function(y, alpha, beta) {
  prod( (beta^alpha)/gamma(alpha) * y^(alpha - 1) * exp(-beta*y))
}

## Priors
B <- 40

prior_alpha <- function(alpha, B = 40){
  (1/B) * ((0 < alpha) * (alpha < B))
}
prior_beta <- function(beta, alpha_0 = 0.43, beta_0 = 1/2) {
  dbeta(beta, alpha_0, beta_0)
}

## Full conditional
cond_beta <- function(beta, alpha_n, beta_n) {
  dbeta(beta, alpha_n, beta_n)
}

## Proposal
J_alpha <- function(alpha, min, max) {
  dunif(alpha, min, max)
}

J_beta <- function(beta, min, max) {
  dunif(beta, min, max)
}

## MLE
MLE_gamma <- function(r, alpha, beta, y) {
  n <- length(y)
  mu <- c(alpha, beta)
  I <- matrix(c(trigamma(alpha), -(1/beta), 
                -(1/beta), alpha/(beta^2)), nrow = 2, byrow = TRUE)
  Sigma <- (1/n)*solve(I)
  rmvnorm(r, mu, Sigma)
}


## Starting values

### alpha and beta
param_init <- (matrix(c(alpha_MLE, beta_MLE), 
                     nrow = NUM_CHAINS, ncol = 2, 
                     byrow = TRUE)*seq(1/4, 3.5, length.out = NUM_CHAINS)) %>%
  as.data.frame() %>% `colnames<-`(c("alpha", "beta"))

## Delta
delta_alpha <- 2
delta_beta  <- 0.25

```

```{r echo=TRUE, fig.align="center", fig.height=4, fig.width=8, message=FALSE, warning=FALSE, cache=TRUE, results='asis'}

# Metropolis-within-Gibbs
for (numChain in 1:NUM_CHAINS) {

  SAMPS <- matrix(nrow = 1 + BURN + M, ncol = 2*length(paramNames)) %>%
    data.frame() %>%
    `colnames<-`(c(paramNames, sprintf("accept.%s", paramNames)))

  initVals <- param_init[numChain, ]
  
  SAMPS[1, paramNames] <- initVals

  for (m in 2:(BURN + M + 1)) {

    # Setup | General
    theta_m <- unlist(select(SAMPS, paramNames)[m - 1, ])
    alpha_m <- theta["alpha"]; beta_m <- theta["beta"]
    
    # alpha
    
    ## Propose new alpha, a.k.a., alpha*
    lwr_lim_alpha_m <- max(alpha_m - delta_alpha, 0)
    upr_lim_alpha_m <- min(alpha_m + delta_alpha, B)
    
    alpha_star <- runif(1, lwr_lim_alpha_m, upr_lim_alpha_m)
    
    ## Accept/reject
    lwr_lim_alpha_star <- max(alpha_star - delta_alpha, 0)
    upr_lim_alpha_star <- min(alpha_star + delta_alpha, B)
    
    tau_alpha_num <- prod(lik(y, alpha_star, beta_m), 
                          prior_alpha(alpha_star),
                          J_alpha(alpha_m, lwr_lim_alpha_star, upr_lim_alpha_star))
    tau_alpha_den <- prod(lik(y, alpha_m, beta_m), 
                          prior_alpha(alpha_m),
                          J_alpha(alpha_star, lwr_lim_alpha_m, upr_lim_alpha_m))
    tau_alpha <- tau_alpha_num/tau_alpha_den
    
    b_alpha <- rbernoulli(1, min(tau_alpha, 1))
    SAMPS[m, sprintf("accept.%s", "alpha")] <- b_alpha
    
    alpha_mplus1 <- alpha_star*b_alpha + (1 - b_alpha)*alpha_m
    
    
    # beta
    
    ## Propose new beta, a.k.a., beta*
    lwr_lim_beta_m <- max(beta_m - delta_beta, 0)
    upr_lim_beta_m <- beta_m + delta_beta
    
    beta_star <- runif(1, lwr_lim_beta_m, upr_lim_beta_m)
    
    ## Accept/reject
    lwr_lim_beta_star <- max(beta_star - delta_beta, 0)
    upr_lim_alpha_star <- beta_star + delta_beta
    
    tau_beta_num <- prod(cond_beta(beta_star, alpha_0 + n*alpha_star, beta_0),
                         J_beta(beta_m, lwr_lim_beta_star, upr_lim_beta_star))
    tau_beta_den <- prod(,
                         J_beta(beta_star, lwr_lim_beta_m, upr_lim_beta_m))
    tau_beta <- tau_beta_num/tau_beta_den
    
    b_alpha <- rbernoulli(1, min(tau_alpha, 1))
    SAMPS[m, sprintf("accept.%s", "alpha")] <- b_alpha
    
    alpha_mplus1 <- alpha_star*b_alpha + (1 - b_alpha)*alpha_m
    
    
    ## Setup
    θk <- θ[paramNames[k]]
    δk <- δ[k]
    f_cond <- f_conds[[k]]
    
    # Propose new θ, a.k.a., θ*
    θk_star <- runif(1, max(θk - δk, 0), θk + δk)
    θ_star <- θ; θ_star[k] <- θk_star

    # Accept/reject
    r <- f_cond(y, θ_star)/f_cond(y, θ)

    b <- rbernoulli(1, min(r, 1))
    SAMPS[m, sprintf("accept.%s", paramNames[k])] <- b

    θk_mplus1 <- θk_star*b + (1 - b)*θk

    θ[k] <- θk_mplus1
    
    
    # beta

    for (k in 1:length(paramNames)) {

      # Setup
      θk <- θ[paramNames[k]]
      δk <- δ[k]
      f_cond <- f_conds[[k]]

      # Propose new θ, a.k.a., θ*
      θk_star <- runif(1, max(θk - δk, 0), θk + δk)
      θ_star <- θ; θ_star[k] <- θk_star

      # Accept/reject
      r <- f_cond(y, θ_star)/f_cond(y, θ)

      b <- rbernoulli(1, min(r, 1))
      SAMPS[m, sprintf("accept.%s", paramNames[k])] <- b

      θk_mplus1 <- θk_star*b + (1 - b)*θk

      θ[k] <- θk_mplus1

    }

    SAMPS[m, paramNames] <- θ

  }
  
  accept_ratio <- SAMPS %>% summarise_at(sprintf("accept.%s", paramNames), mean, na.rm = TRUE)
  
  autoCorr   <- list()
  tracePlots <- list()
  for(k in 1:length(paramNames)) {
    
    timeSeries <- cbind(iter = 1:nrow(SAMPS), select(SAMPS, paramNames[k])) %>%
      filter(iter > 1 + BURN)
    
    autoCorr[[paramNames[k]]] <- acf(pull(timeSeries, paramNames[k]), plot = FALSE)
    tracePlots[[paramNames[k]]] <- ggplot(timeSeries) + 
      geom_line(aes_string(x = "iter", y = paramNames[k])) + theme_bw() + 
      labs(title = sprintf("Traceplot for chain %f, parameter %s", numChain, paramNames[k]))
                                         
    
  }

  output[[numChain]] <- list(chain = SAMPS, initVals = initVals, 
                             accept_ratio = accept_ratio, 
                             tracePlots = tracePlots, acf = autoCorr)


}

```



## Delete




```{r echo=TRUE, fig.align="center", fig.height=4, fig.width=8, message=FALSE, warning=FALSE, cache=TRUE, results='asis'}

# General
M <- 1e4
B <- 250
NUM_CHAINS <- 10
output <- list()

# Weather study
CloudData <- read_csv("CloudData.csv")
yc <- pull(CloudData, UnseededRain); yt <- pull(CloudData, SeededRain)
y <- list(yc = yc, yt = yt)
paramNames <- c("α", "μc", "θ")

## Distributions

### Likelihood
lik  <- function(y, params) {
  
  α <- params["α"]; μc <- params["μc"]; θ <- params["θ"]
  yc <- y[["yc"]]; yt <- y[["yt"]]
  
  yc_bar <- mean(yc); yt_bar <- mean(yt)
  log_yc_bar <- mean(log(yc)); log_yt_bar <- mean(log(yt))
  n <- length(yc) + length(yt)
  
  num <- α^(n*α) * exp((α*n/2) * (log_yc_bar + log_yt_bar - (1/μc) * (yc_bar + yt_bar*(1/θ))))
  den <- gamma(α)^n * μc^(α*n) * θ^(α*n/2) * exp((n/2)*(log_yc_bar + log_yt_bar))
  
  num/den

}

## Full conditionals
f_α  <- function(y, params) {
  lik(y, params)*(1/(3 - 0.1))
}
f_μc <- function(y, params) {
  lik(y, params)*(1/params["μc"])
}
f_θ  <- function(y, params) {
  lik(y, params)*(1/params["θ"])
}

f_conds <- c(f_α, f_μc, f_θ)
 

## Starting values
α0  <- seq(0.3, 0.7, length.out = NUM_CHAINS) # ahat <- 0.5971874
μc0 <- seq(155, 175, length.out = NUM_CHAINS) # 164.5731
θ0  <- seq(1, 6, length.out = NUM_CHAINS)     # 1.33, 5.45
param_init <- data.frame(α = α0, μc = μc0, θ = θ0)

## Delta
δ <- c(0.3, 100, 2.5) 


# Metropolis-within-Gibbs
for (numChain in 1:NUM_CHAINS) {

  SAMPS <- matrix(nrow = 1 + B + M, ncol = 2*length(paramNames)) %>%
    data.frame() %>%
    `colnames<-`(c(paramNames, sprintf("accept.%s", paramNames)))

  initVals <- param_init[numChain, ]
  
  SAMPS[1, paramNames] <- initVals

  for (m in 2:(B + M + 1)) {

    θ <- unlist(select(SAMPS, paramNames)[m - 1, ])

    for (k in 1:length(paramNames)) {

      # Setup
      θk <- θ[paramNames[k]]
      δk <- δ[k]
      f_cond <- f_conds[[k]]

      # Propose new θ, a.k.a., θ*
      θk_star <- runif(1, max(θk - δk, 0), θk + δk)
      θ_star <- θ; θ_star[k] <- θk_star

      # Accept/reject
      r <- f_cond(y, θ_star)/f_cond(y, θ)

      b <- rbernoulli(1, min(r, 1))
      SAMPS[m, sprintf("accept.%s", paramNames[k])] <- b

      θk_mplus1 <- θk_star*b + (1 - b)*θk

      θ[k] <- θk_mplus1

    }

    SAMPS[m, paramNames] <- θ

  }
  
  accept_ratio <- SAMPS %>% summarise_at(sprintf("accept.%s", paramNames), mean, na.rm = TRUE)
  
  autoCorr   <- list()
  tracePlots <- list()
  for(k in 1:length(paramNames)) {
    
    timeSeries <- cbind(iter = 1:nrow(SAMPS), select(SAMPS, paramNames[k])) %>%
      filter(iter > 1 + B)
    
    autoCorr[[paramNames[k]]] <- acf(pull(timeSeries, paramNames[k]), plot = FALSE)
    tracePlots[[paramNames[k]]] <- ggplot(timeSeries) + 
      geom_line(aes_string(x = "iter", y = paramNames[k])) + theme_bw() + 
      labs(title = sprintf("Traceplot for chain %f, parameter %s", numChain, paramNames[k]))
                                         
    
  }

  output[[numChain]] <- list(chain = SAMPS, initVals = initVals, 
                             accept_ratio = accept_ratio, 
                             tracePlots = tracePlots, acf = autoCorr)


}

```



